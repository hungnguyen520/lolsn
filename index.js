var express = require('express')
var app = express();
const port = 3003;
const axios = require('axios');
const fs = require('fs');

const sendCode = (authToken, code, debound) => new Promise((resolve, reject) => {
  axios({
    method: 'POST',
    url: 'https://bargain.lol.garena.vn/api/enter',
    data: {
      code: code.trim(),
      confirm: true
    },
    headers: {
      'Content-Type': 'application/json',
      'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) LeagueOfLegendsClient/11.15.388.2387 (CEF 74) Safari/537.36',
      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
      'Token': authToken,
      'Sec-Fetch-Site': 'same-origin',
      'Sec-Fetch-Mode': 'cors',
      'Sec-Fetch-Dest': 'empty',
      'Accept-Encoding': 'gzip, deflate, br',
      'Accept-Language': 'en-US,en;q=0.9,vi;q=0.8',
      'Via': '1.1 vegur',
      'Referer': 'https://bargain.lol.garena.vn/?token=' + authToken
    }
  })
  .then((res) => {
    setTimeout(() => {
      resolve(res);
    }, debound)
  })
  .catch((err) => {
    setTimeout(() => {
      reject(err);
    }, debound)
  })
});

function readCodes() {
  return fs.readFileSync('code.txt').toString().split("\n");
}

function findAuthToken() {
  var token = '';
  fs.readdirSync('logs').forEach(file => {
    if(!token) {
      const text = fs.readFileSync('logs/' + file).toString();
      const regx = /\/\?token=(.+?)[#"]/g;
      const match = regx.exec(text)
      if(match) {
        token = match[1]; 
      }
    }
  });
  return token;
}

function randomRange(min, max) {
  const ran = Math.random() * (max - min) + min;
  return Math.round(ran);
}

const sendAllCodes = async () => {
  const authToken = findAuthToken();
  if(!authToken) {
    console.log('===>>> Error cannot find authToken !!');
    return;
  }
  console.log('===>>> authToken: ', authToken);
  const codes = readCodes();
  const debound = 200;
  const numOfCodes = codes.length;
  const startIndex = randomRange(0, 65) * 1000;
  let gotta = 0;
  for(var i = startIndex; i < numOfCodes; i++) {
    const code = codes[i];
    try {
      const resData = await sendCode(authToken, code, debound);
      if(resData.data.error === 'ERROR__ENTER_CODE_AMOUNT_OUT_OF_QUOTA') {
        console.log('===>>> YAY! You have enough rewards! <<<===');
        return;
      }
      if(!resData.data.error) {
        gotta++;
      }
      console.log(`SENDING CODE ==>> numOfCodes: ${numOfCodes} - ${code} (${i}) - Gotta: ${gotta} - data: `, resData.data);
    } catch (err) {
      console.error('ERROR !!!! ', err);
    }
  }
}

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
  sendAllCodes();
})